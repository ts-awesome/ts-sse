import {IHttpResponse} from '@ts-awesome/rest';

export type SseField = 'event' | 'data' | 'id' | 'retry'; // keyof fieldBufs

export type SseValue = Buffer | any;

export type SseSerializer = (value: any) => string | Buffer;

export interface ISseBlockConfiguration extends Partial<Record<SseField, SseValue>> {
}

export interface ISseMiddlewareOptions {
  serializer: SseSerializer;
  flushHeaders: boolean;
  keepAliveInterval: false | number;
  flushAfterWrite: boolean;
}


export interface ISseExtension {
  pushData(data: SseValue, id?: string): void;
  pushEvent(event: string, data: SseValue, id?: string): void;
  pushComment(comment: string): void;
}

export interface ISseResponse extends IHttpResponse, ISseExtension {
}
