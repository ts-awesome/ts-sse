import {ISseBlockConfiguration, SseField, SseSerializer, SseValue} from './interfaces';

/**
 * @see
 */

const FIELDS = {
  __comment__: Buffer.from(': '),
  event: Buffer.from('event: '),
  data: Buffer.from('data: '),
  id: Buffer.from('id: '),
  retry: Buffer.from('retry: ')
};

const EOL = Buffer.from('\n');

export function instruction(field: SseField, value: SseValue, serializer?: SseSerializer): Buffer {
  return Buffer.concat([
    FIELDS[field], toBuffer(value, serializer), EOL
  ]);
}

export function comment(comment: string): Buffer { // tslint:disable-line:no-shadowed-variable
  return instruction('__comment__' as SseField, comment, String);
}

export function block(instructions: ISseBlockConfiguration, serializer?: SseSerializer): Buffer {
  const lines = Object.keys(instructions).map((field: SseField) => {
    const fieldSerializer = field === 'data' ? serializer : String;

    return instruction(
      field as SseField,
      toBuffer(instructions[field], fieldSerializer)
    );
  });

  return Buffer.concat([...lines, EOL]);
}

export function message(
  event: string | null,
  data: SseValue,
  id?: string,
  serializer?: SseSerializer
): Buffer {
  const frame: ISseBlockConfiguration = {};

  if (id != null) {
    frame.id = id;
  }

  if (event != null) {
    frame.event = event;
  }

  if (data === undefined) {
    throw new Error('The `data` field in a message is mandatory');
  }

  frame.data = data;

  return block(frame, serializer);
}

function toBuffer(value: SseValue, serializer: SseSerializer = JSON.stringify) {
  if (Buffer.isBuffer(value)) {
    return value;
  }

  const serialized = serializer(value);

  return Buffer.isBuffer(serialized)
    ? serialized
    : Buffer.from(serialized);
}
