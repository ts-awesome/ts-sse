import {IHttpRequest, IHttpResponse, IMiddleware} from '@ts-awesome/rest';
import {ISseMiddlewareOptions, SseValue} from './interfaces';
import {comment, message} from "./utils";
import { injectable } from 'inversify';

@injectable()
export class SSEMiddleware implements IMiddleware {

  private static config: ISseMiddlewareOptions = {
    keepAliveInterval: 5000,
    flushHeaders: true,
    flushAfterWrite: false,
    serializer: JSON.stringify,
  };

  public static configure(config: Partial<ISseMiddlewareOptions>) {
    this.config = {
      ...this.config,
      ...config
    }
  }

  public async handle(req: IHttpRequest, res: IHttpResponse): Promise<void> {
    const {keepAliveInterval, flushHeaders, flushAfterWrite, serializer} = SSEMiddleware.config;

    res.writeHead(200, {
      'Content-Type': 'text/event-stream',
      'Cache-Control': 'no-cache, no-transform',
      'X-Accel-Buffering': 'no',
      'Connection': 'keep-alive',
    });

    res.write(':ok\n\n');

    if (flushHeaders) {
      res.flushHeaders();
    }

    // Start heartbeats (if not disabled)
    if (keepAliveInterval !== false) {
      if (typeof keepAliveInterval !== 'number') {
        throw new Error('keepAliveInterval must be a number or === false');
      }

      startKeepAlives(keepAliveInterval);
    }

    (<any>res).pushData = (data: SseValue, id?: string) => write(message(null, data, id, serializer));
    (<any>res).pushEvent = (event: string, data: SseValue, id?: string) => write(message(event, data, id, serializer));
    (<any>res).pushComment = (text: string) => write(comment(text));

    function write(chunk: any) {
      res.write(chunk);
      if (flushAfterWrite) {
        res.flushHeaders();
      }
    }

    function startKeepAlives(interval: number) {
      // Regularly send keep-alive SSE comments, clear interval on socket close
      const keepAliveTimer = setInterval(() => write(': sse-keep-alive\n'), interval);

      // When the connection gets closed (close=client, finish=server), stop the keep-alive timer
      res.once('close', () => clearInterval(keepAliveTimer));
      res.once('finish', () => clearInterval(keepAliveTimer));
    }
  }

}
